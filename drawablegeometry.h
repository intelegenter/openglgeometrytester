#ifndef DRAWABLEGEOMETRY_H
#define DRAWABLEGEOMETRY_H
#include "geometry.h"
#include <QMatrix4x4>

class QOpenGLBuffer;
class QOpenGLShaderProgram;
class QOpenGLVertexArrayObject;
class QPaintDevice;
class QOpenGLWidget;

class  DrawableGeometry : public Geometry
{
public:
  DrawableGeometry() = default;
  DrawableGeometry(const Geometry &geom);
  DrawableGeometry(std::vector<QVector3D> vertices, Index indices);
  ~DrawableGeometry() override;
  void initialization(QOpenGLWidget* currentView);
  void draw(QPaintDevice *paintDevice, QColor color);

  QVector3D getPosition() const;
  void setPosition(const QVector3D &value);
  Vertex getDataBuffer();


private:
  QVector3D position;
  QQuaternion rotation;
  QMatrix4x4 model;
  QOpenGLShaderProgram* m_shader;
  QOpenGLVertexArrayObject* m_vao;
  QOpenGLBuffer* m_vbo;
//  QOpenGLBuffer* m_nbo;
  QOpenGLBuffer* m_ibo;
  QOpenGLWidget* m_currentView;

  void updateTransform();
};

#endif // DRAWABLEGEOMETRY_H

