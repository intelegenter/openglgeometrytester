#include "geometry.h"
#include <algorithm>

Geometry::Geometry(Vertex vertices, Index indices)
{
  setVertices(vertices);
  setIndices(indices);
}

Geometry::~Geometry()
{

}

const Vertex &Geometry::vertices() const
{
  return m_vertices;
}

void Geometry::setVertices(const Vertex &vertices)
{
  m_vertices = vertices;
}

void Geometry::appendVertices(const Vertex &vertices)
{
  std::copy(vertices.begin(), vertices.end(), std::back_inserter(m_vertices));
}

void Geometry::appendIndices(const Index &indices)
{
  std::copy(indices.begin(), indices.end(), std::back_inserter(m_indices));
  lastIndex = maxInd(indices);
}

void Geometry::appendNormals(const Normal &normals)
{
  std::copy(normals.begin(), normals.end(), std::back_inserter(m_normals));
}

const Index& Geometry::indices() const
{
  return m_indices;
}

void Geometry::setIndices(const Index &indices)
{
  m_indices = indices;
  lastIndex = maxInd(indices);
}

const unsigned int& Geometry::getLastIndex() const
{
  return lastIndex;
}

const Normal& Geometry::normals() const
{
  return m_normals;
}

void Geometry::setNormals(const Normal &normals)
{
  m_normals = normals;
}

unsigned int Geometry::maxInd(const Index &indices) const
{
  if(indices.empty())
    return -1;
  unsigned int max = indices.front();
  for(const auto& ind : indices)
  {
    if(ind > max)
      max = ind;
  }
  return max;
}
