#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "glwindow.h"
#include <QDockWidget>

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  QDockWidget * const dock2 = new QDockWidget(ui->centralwidget);
  GLWindow * const widget2 = new GLWindow(ui->centralwidget);
  dock2->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
  dock2->setWidget(widget2);
  addDockWidget(Qt::LeftDockWidgetArea, dock2);
  resizeDocks({dock2}, {maximumWidth()}, Qt::Horizontal);


}

MainWindow::~MainWindow()
{
  delete ui;
}

