#ifndef GEOMETRYCREATOR_H
#define GEOMETRYCREATOR_H
#include "geometry.h"

class GeometryCreator
{
public:
  GeometryCreator();
  static Geometry createTube(float radius, int circleSectionsCount, Spline spline);
  static Geometry createCube(float side);
  static Geometry extrude(Spline spline, QQuaternion tailRotation, QVector3D extrudeVector, QVector3D startPos);
};

#endif // GEOMETRYCREATOR_H
