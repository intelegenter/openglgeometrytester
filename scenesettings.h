#ifndef SCENESETTINGS_H
#define SCENESETTINGS_H
#include <QVector3D>
#include <QMatrix4x4>
#include <QColor>

class SceneSettings
{
public:
  SceneSettings();
  static SceneSettings* get();

private:
  static SceneSettings* instance;

public:
  QVector3D lightPos;
  QColor lightColor;
//  QMatrix4x4 cameraTransform;
  QMatrix4x4 projection;
  QMatrix4x4 screenScale;
  QMatrix4x4 screenTranslation;
  QMatrix4x4 cameraRotation;
  QMatrix4x4 cameraTranslation;

};

#endif // SCENESETTINGS_H
