#ifndef GEOMETRY_H
#define GEOMETRY_H
#include <vector>
#include <QVector3D>

using Spline = std::vector<QVector3D>;
using Vertex = Spline;
using Normal = Spline;
using Index = std::vector<unsigned int>;

class Geometry
{
public:
  Geometry() = default;
  Geometry(Vertex vertices, Index indices);
  virtual ~Geometry();

  const Vertex &vertices() const;
  void setVertices(const Vertex &vertices);
  void appendVertices(const Vertex &vertices);
  void appendIndices(const Index &indices);
  void appendNormals(const Normal &normals);

  const Index& indices() const;
  void setIndices(const Index &indices);

  const unsigned int &getLastIndex() const;
  const Normal& normals() const;
  void setNormals(const Normal &normals);

protected:
  Vertex m_vertices;
  Index m_indices;
  Normal m_normals;
  unsigned int lastIndex = -1;
  unsigned int maxInd(const Index &indices) const;

};

#endif // GEOMETRY_H
