#include "mainwindow.h"
#include <QSurfaceFormat>

#include <QApplication>

int main(int argc, char *argv[])
{
//  auto fmt = QSurfaceFormat::defaultFormat();
//  fmt.setDepthBufferSize(24);
////  fmt.setDepthBufferSize(4);  // 4-byte depth
////  fmt.setSamples(4);          // 4x MSAA
////    fmt.setRenderableType(QSurfaceFormat::OpenGLES);
////    fmt.setVersion(3,2);
//  fmt.setVersion(4,5);
//  QSurfaceFormat::setDefaultFormat(fmt);

    QSurfaceFormat format;
        format.setDepthBufferSize(24);
//        format.setVersion(3,3);
//        format.setOption(QSurfaceFormat::DebugContext, true);
        QSurfaceFormat::setDefaultFormat(format);


  QApplication a(argc, argv);
  MainWindow w;
  w.show();
  return a.exec();
}
