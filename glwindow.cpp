#include "glwindow.h"
#include "geometrycreator.h"
#include "drawablegeometry.h"
#include "scenesettings.h"
#include <QOpenGLContext>
#include <QOpenGLExtraFunctions>
#include <QWheelEvent>
#include <QPainter>
#include <QTransform>

GLWindow::GLWindow(QWidget *parent)
  : QOpenGLWidget(parent)
{
  Spline line = {{0, 0, 0},
                 {0, 0.5, 0},
                 {0.5, 0.5, 0},
                 {0.5, 0, 0}};
//  Spline line =
//  {{0, 0, 0},
//  {-0.301184, 1.19202, 2.7093},
//  {-0.40207, 1.35932, 2.9924},
//  {-0.542542, 1.45987, 3.2758},
//  {-0.625941, 1.59536, 3.5584},
//  {-0.707425, 1.76641, 3.8409},
//  {-0.833263, 1.93858, 4.1218},
//  {-0.895895, 2.11743, 4.4049},
//  {-0.914741, 2.31069, 4.6869},
//  {-1.0024, 2.48522, 4.9691},
//  {-1.20376, 2.7133, 5.2526}};

  tube = new DrawableGeometry(GeometryCreator::createTube(0.2, 100, line));
  cube = new DrawableGeometry(GeometryCreator::createCube(0.5));
  auto settings = SceneSettings::get();
  settings->cameraTranslation.translate(0.f, 0.f, 1.0 / zoomLvl);
  settings->lightPos = QVector3D(0, 100, -100);
  settings->lightColor = QColor(255, 255, 255, 255);
}

void MessageCallback( GLenum source,
                 GLenum type,
                 GLuint id,
                 GLenum severity,
                 GLsizei length,
                 const GLchar* message,
                 const void* userParam )
{
  if (type == GL_DEBUG_TYPE_ERROR)
    qCritical() << type << severity << message;
  else
    qDebug() << type << severity << message;

//  fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
//           ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
//            type, severity, message );
}

void GLWindow::initializeGL()
{
  initializeOpenGLFunctions();

  glEnable( GL_DEBUG_OUTPUT );
  glDebugMessageCallback( MessageCallback, 0 );
  tube->initialization(this);
  cube->initialization(this);
}

void GLWindow::paintGL()
{
  glClearColor(0.3, 0.3, 0.3, 1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
//  glEnable(GL_LIGHTING);

  tube->draw(this, Qt::red);
  cube->draw(this, Qt::blue);
}

void GLWindow::resizeGL(int width, int height)
{
  m_width = width;
  m_height = height;

  auto settings = SceneSettings::get();

  settings->projection.setToIdentity();
  settings->projection.perspective(90, m_width / m_height, 0.01, 1000.f);

  settings->screenScale.setToIdentity();
  settings->screenScale.scale(m_width, -m_height, 1);
  settings->screenTranslation.setToIdentity();
  settings->screenTranslation.translate(m_width / 2, m_height / 2, 0);
}

void GLWindow::wheelEvent(QWheelEvent *event)
{
  auto settings = SceneSettings::get();
  if(event->modifiers().testFlag(Qt::ControlModifier))
  {
    m_lightPitch += event->delta() > 0.f ? 10.f: -10.f;
    lightRotation.setToIdentity();
    lightRotation.rotate(-m_lightPitch, {0.f, 1.f, 0.f});
    lightRotation.rotate(-m_lightYaw, lightRotation.inverted() * QVector3D(1.f, 0.f, 0.f));
    settings->lightPos = lightRotation * QVector3D(0.f, 0.f, 100.f);
  }
  else if(event->modifiers().testFlag(Qt::ShiftModifier))
  {
    m_lightYaw += event->delta() > 0.f ? 10.f: -10.f;
    lightRotation.setToIdentity();
    lightRotation.rotate(-m_lightPitch, {0.f, 1.f, 0.f});
    lightRotation.rotate(-m_lightYaw, lightRotation.inverted() * QVector3D(1.f, 0.f, 0.f));
    settings->lightPos = lightRotation * QVector3D(0.f, 0.f, 100.f);
  }
  else
  {
    zoomLvl *= event->delta() > 0.f ? 1.1f : 0.9f;
    qDebug() << zoomLvl;
    settings->cameraTranslation.setToIdentity();
    settings->cameraTranslation.translate(QVector3D(0, 0, 1/zoomLvl));
  }
  update();
  QOpenGLWidget::wheelEvent(event);
}

void GLWindow::mouseMoveEvent(QMouseEvent *event)
{
  auto settings = SceneSettings::get();
  if(m_inRotating)
  {
    auto difference = event->pos() - oldPos;
    m_pitch += difference.x();
    m_yaw += difference.y();
    settings->cameraRotation.setToIdentity();
    settings->cameraRotation.rotate(-m_pitch, {0.f, 1.f, 0.f});
    settings->cameraRotation.rotate(-m_yaw, settings->cameraRotation.inverted() * QVector3D(1.f, 0.f, 0.f));

    oldPos = event->pos();
    update();
  }
  QOpenGLWidget::mouseMoveEvent(event);
}

void GLWindow::mousePressEvent(QMouseEvent *event)
{
  if(event->button() == Qt::LeftButton)
  {
    m_inRotating = true;
    oldPos = event->pos();
  }
  QOpenGLWidget::mousePressEvent(event);
}

void GLWindow::mouseReleaseEvent(QMouseEvent *event)
{
  if(event->button() == Qt::LeftButton)
  {
    m_inRotating = false;
  }
  QOpenGLWidget::mouseReleaseEvent(event);
}
