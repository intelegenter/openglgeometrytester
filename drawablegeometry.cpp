#include "drawablegeometry.h"
#include "scenesettings.h"
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLContext>
#include <QOpenGLExtraFunctions>
#include <QPainter>
#include <QPaintDevice>
#include <QOpenGLWidget>

DrawableGeometry::DrawableGeometry(const Geometry &geom)
  : Geometry(geom)
{
}

DrawableGeometry::DrawableGeometry(std::vector<QVector3D> vertices, Index indices)
  : Geometry(vertices, indices)
{
}

DrawableGeometry::~DrawableGeometry()
{
  if(!m_currentView)
    return;
  m_currentView->makeCurrent();
  m_vbo->destroy();
  m_ibo->destroy();
  m_vao->destroy();

  delete m_vbo;
  delete m_vao;
  delete m_ibo;
  delete m_shader;
}

void DrawableGeometry::initialization(QOpenGLWidget *currentView)
{
  assert(currentView);
  m_currentView = currentView;
  m_shader = new QOpenGLShaderProgram();
  // Compile vertex shader
  assert (m_shader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/vshader.glsl"));
  // Compile fragment shader
  assert (m_shader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/fshader.glsl"));
  // Link shader pipeline
  assert (m_shader->link());

  m_vao = new QOpenGLVertexArrayObject();
  m_vbo = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
//  m_nbo = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  m_ibo = new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
  m_vao->create();
  m_vbo->create();
//  m_nbo->create();
  m_ibo->create();

  m_vao->bind();

  // Bind shader pipeline for use
  assert (m_shader->bind());

//  m_nbo->bind();
//  m_nbo->allocate(m_normals.data(), m_normals.size() * sizeof(QVector3D));

  m_vbo->bind();
  auto dataBuffer = getDataBuffer();
  m_vbo->allocate(dataBuffer.data(), dataBuffer.size() * sizeof(QVector3D));

  m_ibo->bind();
  m_ibo->allocate(m_indices.data(), m_indices.size() * sizeof(unsigned int));

  m_shader->enableAttributeArray(0);
  m_shader->setAttributeBuffer(0, GL_FLOAT, 0, 3, sizeof(QVector3D) * 2);
  m_shader->enableAttributeArray(1);
  m_shader->setAttributeBuffer(1, GL_FLOAT, sizeof(QVector3D), 3, sizeof(QVector3D) * 2);

  m_vao->release();
  m_shader->release();
}

void DrawableGeometry::draw(QPaintDevice *paintDevice, QColor color)
{
    auto settings = SceneSettings::get();
    m_vao->bind();

    assert (m_shader->bind());

    m_shader->setUniformValue("model", model);
    m_shader->setUniformValue("objectColor", color);
    m_shader->setUniformValue("projection", settings->projection);
    m_shader->setUniformValue("camTranslation", settings->cameraTranslation);
    m_shader->setUniformValue("camRotation", settings->cameraRotation);
    m_shader->setUniformValue("lightColor", settings->lightColor);
    m_shader->setUniformValue("lightPos", settings->lightPos);

    glDrawElements(GL_TRIANGLES, indices().size(), GL_UNSIGNED_INT, nullptr);
    m_vao->release();
}

QVector3D DrawableGeometry::getPosition() const
{
  return position;
}

void DrawableGeometry::setPosition(const QVector3D &value)
{
  position = value;
  updateTransform();
}

Vertex DrawableGeometry::getDataBuffer()
{
  assert(m_normals.size() == m_vertices.size());
  Vertex newDataBuffer;
//  newDataBuffer.reserve(m_normals.size() * 2);
  for(int i = 0; i < m_normals.size(); i++)
  {
    newDataBuffer.push_back(m_vertices[i]);
    newDataBuffer.push_back(m_normals[i]);
  }
  return newDataBuffer;
}

void DrawableGeometry::updateTransform()
{
  model.setToIdentity();
  model.rotate(rotation);
  model.translate(position);
}

