#include "geometrycreator.h"
#include <algorithm>
#include <QMatrix4x4>

GeometryCreator::GeometryCreator()
{

}

Geometry GeometryCreator::createTube(float radius, int circleSectionsCount, Spline spline)
{
  Geometry newGeometry;
  if(spline.empty())
    return newGeometry;
  Spline circle;
  //tube form (circle) generation
  QVector3D circleNormal = {0, 0, 1};
  {
    double angle = 0;
    double step = 360.0 / (double)circleSectionsCount;
    QVector3D pt = {radius, 0, 0};
    QMatrix4x4 rotMatr;
    rotMatr.rotate(step, circleNormal);
    circle.push_back(pt);
    angle += step;

    while(angle < 360.0)
    {
      pt = rotMatr * pt;
      circle.push_back(pt);
      angle += step;
    }
    //end form (circle) generation

    //little exit condition
    if(spline.size() < 2)
    {
      newGeometry.setVertices(circle);
      return newGeometry;
    }
  }

  QVector3D normal = (spline[1] - spline[0]).normalized();
  //direction of first section
  {
    auto firstSectionRotation = QQuaternion::rotationTo(circleNormal, normal);

    //first section
    Spline firstSection;
    firstSection.assign(circle.begin(), circle.end());

    for(auto& vec : firstSection)
    {
      vec = firstSectionRotation.rotatedVector(vec);
    }
    Normal normals;
    for(int i = 0; i < firstSection.size(); i++)
    {
      normals.push_back(-normal);
    }
    newGeometry.setVertices(firstSection);
    newGeometry.setNormals(normals);
  }

  //other sections
  uint64_t lastInd = spline.size() - 1;
  QVector3D lastNormal = normal;
  for(uint64_t i = 1; i < spline.size(); i++)
  {
    Geometry newGeom;
    Spline form(newGeometry.vertices().end() - circle.size(), newGeometry.vertices().end());
    if(i == lastInd)
    {
      QVector3D extrudeVector = spline[i] - spline[i-1];
      QVector3D newNormal = extrudeVector.normalized();
      newGeom = extrude(form, QQuaternion::rotationTo(lastNormal, newNormal), extrudeVector, spline[i-1]);
      lastNormal = newNormal;
      Normal normals;
      for(int i = 0; i < newGeom.vertices().size(); i++)
      {
        normals.push_back(newNormal);
      }
      newGeom.setNormals(normals);
    }
    else
    {
      QVector3D extrudeVector = spline[i] - spline[i-1];
      QVector3D newNormal = (spline[i+1] - spline[i-1]).normalized();
      newGeom = extrude(form, QQuaternion::rotationTo(lastNormal, newNormal), extrudeVector, spline[i-1]);
      lastNormal = newNormal;
      QVector3D center;
      for(int i = 0; i < newGeom.vertices().size(); i++)
      {
        center += newGeom.vertices()[i];
      }
      center /= (double)newGeom.vertices().size();
      Normal normals;
      for(int i = 0; i < newGeom.vertices().size(); i++)
      {
        normals.push_back((newGeom.vertices()[i] - center).normalized());
      }
      newGeom.setNormals(normals);

    }
    newGeometry.appendVertices(newGeom.vertices());
    newGeometry.appendNormals(newGeom.normals());

    Index indices;
    unsigned int lastInd = newGeometry.getLastIndex() + 1;
    indices.assign(newGeom.indices().begin(), newGeom.indices().end());
    if(lastInd != 0)
    {
      for(auto& ind : indices)
      {
        ind += lastInd - circle.size();
      }
    }
    newGeometry.appendIndices(indices);
  }

  //start cap
  {
    newGeometry.appendVertices({spline[0]});
    newGeometry.appendNormals({(spline[0] - spline[1]).normalized()});
    unsigned int centerInd = newGeometry.vertices().size() - 1;
    Index indices;
    int i;
    for(i = 0; i < circle.size()-1; i++)
    {
      indices.push_back(centerInd);
      indices.push_back(i+1);
      indices.push_back(i);
    }
    indices.push_back(centerInd);
    indices.push_back(0);
    indices.push_back(i);
    newGeometry.appendIndices(indices);
  }

  //end cap
  {
    newGeometry.appendVertices({spline.back()});
    newGeometry.appendNormals({(spline.back() - *(spline.end()-1)).normalized()});
    unsigned int centerInd = newGeometry.vertices().size() - 1;
    Index indices;
    int i;
    for(i = 0; i < circle.size()-1; i++)
    {
      indices.push_back(centerInd);
      indices.push_back(i + circle.size() * (spline.size() - 1));
      indices.push_back(i + 1 + circle.size() * (spline.size() - 1));
    }
    indices.push_back(centerInd);
    indices.push_back(i + circle.size() * (spline.size() - 1));
    indices.push_back(0 + circle.size() * (spline.size() - 1));
    newGeometry.appendIndices(indices);
  }

  return newGeometry;
}

Geometry GeometryCreator::createCube(float side)
{
  float halfSide = side/2;
  Geometry newGeom;
  Vertex vertices = {
    //face
    {-halfSide, halfSide, -halfSide},
    {halfSide, halfSide, -halfSide},
    {halfSide, -halfSide, -halfSide},
    {-halfSide, -halfSide, -halfSide},
    //back-face
    {-halfSide, halfSide, halfSide},
    {halfSide, halfSide, halfSide},
    {halfSide, -halfSide, halfSide},
    {-halfSide, -halfSide, halfSide},
  };
  Normal normals = {
    //face
    QVector3D(-halfSide,  halfSide, -halfSide).normalized(),
    QVector3D( halfSide,  halfSide, -halfSide).normalized(),
    QVector3D( halfSide, -halfSide, -halfSide).normalized(),
    QVector3D(-halfSide, -halfSide, -halfSide).normalized(),
    //back-face
    QVector3D(-halfSide,  halfSide, halfSide).normalized(),
    QVector3D( halfSide,  halfSide, halfSide).normalized(),
    QVector3D( halfSide, -halfSide, halfSide).normalized(),
    QVector3D(-halfSide, -halfSide, halfSide).normalized(),
  };
  Index indices = {
    0, 1, 3, 3, 1, 2,//face
    4, 5, 0, 0, 5, 1,//top
    5, 6, 1, 1, 6, 2,//right
    6, 7, 2, 2, 7, 3,//bot
    7, 4, 3, 3, 4, 0,//left
    5, 4, 6, 6, 4, 7,//back
  };
  newGeom.setIndices(indices);
  newGeom.setVertices(vertices);
  newGeom.setNormals(normals);
  return newGeom;
}

Geometry GeometryCreator::extrude(Spline spline, QQuaternion tailRotation, QVector3D extrudeVector, QVector3D startPos)
{
  Geometry newGeom;
  Index indices;
  Spline spline2;
  Vertex vertices;
  spline2.assign(spline.begin(), spline.end());

  for(int i = 0; i < spline.size(); i++)
  {
    spline2[i] = tailRotation.rotatedVector(spline2[i] - startPos) + startPos + extrudeVector;
  }
  int i;
  for(i = 0; i < spline.size() - 1; i++)
  {
    //triangles
    indices.push_back(i);
    indices.push_back(i + 1);
    indices.push_back(i + spline.size());

    indices.push_back(i + spline.size());
    indices.push_back(i + 1);
    indices.push_back(i + spline.size() + 1);
  }

  indices.push_back(i);
  indices.push_back(0);
  indices.push_back(i + spline.size());

  indices.push_back(i + spline.size());
  indices.push_back(0);
  indices.push_back(spline.size());

  newGeom.setIndices(indices);
  newGeom.setVertices(spline2);
  return newGeom;
}
