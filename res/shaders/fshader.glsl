#version 330
in vec3 normal;
in vec3 FragPos;
out vec4 FragColor;
uniform vec3 lightPos;
uniform vec4 lightColor;
uniform vec4 objectColor;
//! [0]
void main()
{
    vec3 lc = vec3(lightColor);
    vec3 oc = vec3(objectColor);
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lc;
    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lc;
    vec3 result = (ambient + diffuse) * oc;
    FragColor = vec4(result, 1.0);
}
