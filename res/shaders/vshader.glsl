#version 330
uniform mat4 mvp_matrix;

//attribute vec4 a_position;
layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_Normal;
uniform mat4x4 projection;
uniform mat4x4 camTranslation;
uniform mat4x4 camRotation;
uniform mat4x4 model;
out vec3 normal;
out vec3 FragPos;

//! [0]
void main()
{
    // Calculate vertex position in screen space
    normal = a_Normal;
    gl_Position = projection * camTranslation * camRotation * model * vec4(a_position, 1);
    FragPos = vec3(model * vec4(a_position, 1.0));
}
//! [0]

//#version 330 core
//layout (location = 0) in vec3 aPos;
//uniform mat4 mvp_matrix;
//void main()
//{
//    gl_Position = mvp_matrix * vec4(aPos, 1.0);
//}
