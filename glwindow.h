#ifndef GLWINDOW_H
#define GLWINDOW_H
#include <QOpenGLWidget>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLExtraFunctions>

class DrawableGeometry;
class GLWindow : public QOpenGLWidget, protected QOpenGLExtraFunctions
{
public:
  GLWindow(QWidget* parent);

  void initializeGL() override;
  void paintGL() override;
  void resizeGL(int width, int height) override;
  void wheelEvent(QWheelEvent *event) override;
  void mouseMoveEvent(QMouseEvent *event) override;
  void mousePressEvent(QMouseEvent *event) override;
  void mouseReleaseEvent(QMouseEvent *event) override;

  DrawableGeometry *tube = nullptr;
  DrawableGeometry *cube = nullptr;

//  QMatrix4x4 screenScale;
//  QMatrix4x4 screenTranslation;
//  QMatrix4x4 viewProjection;
//  QMatrix4x4 cameraRotation;
//  QMatrix4x4 cameraTranslation;
  QMatrix4x4 lightRotation;
  double m_height, m_width, zoomLvl = -1;
  double m_pitch = 0;
  double m_yaw = 0;
  double m_lightPitch = 0;
  double m_lightYaw = 0;
  bool m_inRotating = false;
  QPoint oldPos;

//  QOpenGLShaderProgram program;
};

#endif // GLWINDOW_H
